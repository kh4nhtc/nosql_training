# -*- coding: utf-8 -*-
import unicodedata, codecs
from pymongo import MongoClient
#############################################
client = MongoClient('localhost', 27017) #connext to mongodb
db = client.questionDB	#create a database
question = db.question #create a collection
#############################################

formData = { "question": "",
			 "level": "",
			 "answers": {},
			 "correct_answer": ""
			}

def readData():
	dataLine = unicode(f.readline())
	if dataLine == "\n":
		print "End of data!"
		return "Done"
	if dataLine.find("INSERT") >= 0:
		dataLine = unicode(f.readline())
	flag = dataLine.find(", ")
	dataLine = dataLine[flag+2:][:-3]
	return dataLine

def treatAnswers(string_answers):
	# answer_list = []
	answers = {}
	###################################
	cutAnswers = string_answers.split(",")
	for i in xrange(len(cutAnswers)):
		answers[str(i+1)] = cutAnswers[i].strip()
		# cutAnswers[i] = str(i+1)+":"+cutAnswers[i]
		# answer_list.append(cutAnswers[i])
	###################################
	# return answer_list
	return answers

def convertData():
	for i in xrange(4000):
		data = readData()
		if data == "Done":
			return 0
		#cutQuestion = list ["level, question" | "answers" | "correct_answer"]
		cutQuestion = data.split("', ")
		_cutQuestion = cutQuestion[0].split(", '")
		######################################
		convertedData = formData.copy()
		convertedData["level"] = int(_cutQuestion[0])
		convertedData["question"] = unicode(_cutQuestion[1])
		convertedData["answers"] = treatAnswers(unicode(cutQuestion[1])[1:])
		convertedData["correct_answer"] = int(cutQuestion[2])
		#######################################
		question.insert_one(convertedData)  #insert into db
		print "=========================>inserted question(s) = "+str(i+1)

if __name__ == '__main__':
	source_file = "question.sql"
	f = codecs.open(source_file, 'r', 'utf-8')
	######################################
	for line in f:
		readLine = line
		if readLine.find("INSERT INTO") >= 0:
			convertData()
			break	
	print "Convert Completed!"
	######################################
	f.close()
