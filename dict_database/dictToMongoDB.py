# -*- coding: utf-8 -*-
import unicodedata, codecs
from pymongo import MongoClient

client = MongoClient('localhost', 27017) #connext to mongodb
db = client.vi_dict	#create a database
ja_vi = db.ja_vi #create a collection

##################################
symbol_1 = '∴'.decode('utf-8')
symbol_2 = '☆'.decode('utf-8')
symbol_3 = '◆'.decode('utf-8')
symbol_4 = '※'.decode('utf-8')
symbol_5 = '「'.decode('utf-8')
symbol_6 = '」'.decode('utf-8')
##################################
#form
formData = {"word":"",
			"meaning": []
			}

formWord = {"word":"",
			"viyomi":"",
			"meaning": [],
			"sentences": []
			}

formSentences = {"ja":"",
				 "vi":""
				}


def readData():
	dataLine = unicode(f.readline())
	if dataLine == "\n":
		print "End of data!"
		return "Done"
	if dataLine.find("INSERT") >= 0:
		dataLine = unicode(f.readline())
	flag = dataLine.find(", ")
	dataLine = dataLine[flag+3:][:-4]
	return dataLine

def treatSentences(_sentence):
	sentence = formSentences.copy()
	check = _sentence.find(':')
	if check > 0:
		cutSentence = _sentence.split(':')
		sentence["ja"] = cutSentence[0]
		sentence["vi"] = cutSentence[1]
		return sentence
	else:
		sentence["ja"] = "Ghi chú"
		sentence["vi"] = _sentence
		return sentence

def treatWord(list_string_word):
	listWord = []
	####################################
	pop_list = []
	for i in xrange(len(list_string_word)):
		if list_string_word[i] == '':
			pop_list.append(i)
	for i in xrange(len(pop_list)):
		list_string_word.pop(pop_list[i])
	####################################
	for i in xrange(len(list_string_word)):
		word = formWord.copy()
		#print word
		listSentences = []
		listMeaning = []
		###########################
		flag_1 = list_string_word[i].find(symbol_2)
		flag_2 = list_string_word[i].find(symbol_3)
		if flag_1 < flag_2 and flag_1 > 0:
			cutWord = list_string_word[i].split(symbol_2)
			#insert word xu ly tu cutWord[0]
			flag = cutWord[0].find(symbol_6)
			if flag > 0:
				word["word"] = (cutWord[0])[:flag+1]
				word["viyomi"] = (cutWord[0])[flag+1:]
			cutWord.pop(0)  #con lai list cac kieu {[symbol_2] loai_tu symbol_3 nghia symbol_4 vdu}
			for j in xrange(len(cutWord)):
				_data = {}
				_cutWord = cutWord[j].split(symbol_3)
				key = _cutWord[0]
				_cutWord.pop(0)  #con lai list cac kieu {[symbol_3] nghia symbol_4 vdu}
				for k in xrange(len(_cutWord)):
					__cutWord = _cutWord[k].split(symbol_4)
					_data[key] = __cutWord[0]
					listMeaning.append(_data)
					__cutWord.pop(0)  #con lai list kieu {[symbol_4] vdu}
					for l in xrange(len(__cutWord)):
						sentence = treatSentences(__cutWord[l])
						listSentences.append(sentence)
			###########################
		elif flag_1 > flag_2 and flag_1 > 0:
			cutWord = list_string_word[i].split(symbol_2)
			cutWord_except = cutWord[0].split(symbol_3)
			cutWord_except.pop(0)
			#print cutWord_except
			for j in xrange(len(cutWord_except)):
				_data = {}
				key = "o"
				value = cutWord_except[j]
				_data[key] = value
				listMeaning.append(_data)
			cutWord.pop(0) #con lai list cac kieu {[symbol_2] loai_tu symbol_3 nghia symbol_4 vdu}
			for j in xrange(len(cutWord)):
				_data = {}
				_cutWord = cutWord[j].split(symbol_3)
				key = _cutWord[0]
				_cutWord.pop(0)  #con lai list cac kieu {[symbol_3] nghia symbol_4 vdu}
				for k in xrange(len(_cutWord)):
					__cutWord = _cutWord[k].split(symbol_4)
					_data[key] = __cutWord[0]
					listMeaning.append(_data)
					__cutWord.pop(0)  #con lai list kieu {[symbol_4] vdu}
					for l in xrange(len(__cutWord)):
						sentence = treatSentences(__cutWord[l])
						listSentences.append(sentence)
		elif flag_1 < 0:
			cutWord = list_string_word[i].split(symbol_3)
			flag = cutWord[0].find(symbol_6)
			if flag > 0:
				word["word"] = (cutWord[0])[:flag+1]
				word["viyomi"] = (cutWord[0])[flag+1:]
			cutWord.pop(0) #con lai list cac kieu {[symbol_3] nghia~ symbol_4 vdu}
			for j in xrange(len(cutWord)):
				_data = {}
				_cutWord = cutWord[j].split(symbol_4)
				key = "o"
				value = _cutWord[0]
				_data[key] = value
				listMeaning.append(_data)
				_cutWord.pop(0) #con lai list kieu {[symbol_4] vdu}
				for k in xrange(len(_cutWord)):
					sentence = treatSentences(_cutWord[k])
					listSentences.append(sentence)
		###########################
		word["sentences"] = (listSentences)
		word["meaning"] = (listMeaning)
		listWord.append(word)
		###########################
		#reset lists
		listMeaning = []
		listSentences = []
		#word = None
	####################################
	list_string_word = []
	return listWord

def insertDataMeaning(dataMeaning):
	listDataMeaning = []
	###################################
	cutWord = dataMeaning.split(symbol_1)
	check = dataMeaning.find(symbol_1)
	if check >= 0:
		if cutWord[0] == '':
			cutWord.pop(0)
		for i in xrange(len(cutWord)-1):
				_cutWord = cutWord[i].split(symbol_4)
				check = cutWord[i].find(symbol_4)
				if check >= 0:
					check = _cutWord[-1].find(':')
					if check <= 0:
						cutWord[i] = cutWord[i] + cutWord[i+1]
						cutWord[i+1] = ''
						break
	###################################
	listWord = treatWord(cutWord)
	listDataMeaning = (listWord)
	#reset list
	listWord = []
	return listDataMeaning

def convertData():
	for i in xrange(75000):
		data = readData()
		if data == "Done":
			return 0
		#cut data => list || word_doc_list[0] = word, word_doc_list[1] = content
		word_doc_list = data.split("', '")
		##########################################
		convertedData = formData.copy()
		convertedData["word"] = word_doc_list[0]  #insert word
		listDataMeaning = insertDataMeaning(word_doc_list[1])		  #truyen vao content cua 1 word
		convertedData["meaning"] = (listDataMeaning)  #insert meaning
		##########################################
		ja_vi.insert_one(convertedData)  #insert into db
		print "=========================>inserted word = "+str(i+1)
		#reset list
		listDataMeaning = []
		word_doc_list = []

if __name__ == '__main__':
	source_file = "ja_vi.sql"
	f = codecs.open(source_file, 'r', 'utf-8')
	######################################
	for line in f:
		readLine = line
		if readLine.find("INSERT INTO") >= 0:
			convertData()
			break	
	print "Convert Completed!"
	######################################
	f.close()
